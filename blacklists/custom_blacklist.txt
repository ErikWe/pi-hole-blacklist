# Author: ErikWe
# Hosts: Collection of custom selected hosts
# Title: Custom Blacklist
0.0.0.0 cdn3.gracza.pl
0.0.0.0 commonapi.ai.xiaomi.com
0.0.0.0 commonapi.ai.xiaomi.com
0.0.0.0 diag.meethue.com
0.0.0.0 free2play.gamepressure.com
0.0.0.0 grayconfig.ai.xiaomi.com
0.0.0.0 osb.samsungqbe.com
0.0.0.0 scontent-dus1-1.cdninstagram.com
0.0.0.0 tracker.ai.xiaomi.com
0.0.0.0 tse4.mm.bing.net
0.0.0.0 www.bing.com
0.0.0.0 www.philips-hue.com
0.0.0.0 x1.c.lencr.org
