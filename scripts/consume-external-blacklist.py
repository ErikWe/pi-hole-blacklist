#Copyright (C) 2022 ErikWe
import re

def main():
    printCopyright()
    print ("Consuming external blacklist:")
    print ("Please enter your source blacklist: ")
    source = input("source-blacklist: ")
    sourceEntries = readEntriesFrom(source)
    parseHosts(sourceEntries)
    print ("The following entries are parsed:")
    printEntries(sourceEntries)
    print ("Please enter your target blacklist: ")
    target = input("target-blacklist: ")
    storeHosts(sourceEntries, target)


def printEntries(entriesToPrint):
    for singleEntry in entriesToPrint:
        if singleEntry != "":
            print (singleEntry)


def parseHosts(entriesToParse):
    for i, singleEntry in enumerate(entriesToParse):
        entriesToParse[i] = parseSingleEntry(singleEntry)


def parseSingleEntry(singleEntryToParse):
    #We remove all IP adresses from our entry
    entryWithoutIp = re.sub(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", "", singleEntryToParse)
    #We remove all spaces
    entryWithoutSpaces = re.sub(r"\s+", "", entryWithoutIp)
    #We remove comments
    return re.sub("#.*", "", entryWithoutSpaces)


def storeHosts(entries, target):
    targetEntries = readEntriesFrom(target)
    for singleEntry in entries:
        if singleEntry != "":
            targetEntries += ["0.0.0.0 " + singleEntry]
    targetEntries.sort()
    printEntries(targetEntries)
    writeEntriesTo(targetEntries, target)


def readEntriesFrom(source):
    sourceFile = open(source)
    entries = sourceFile.read().splitlines()
    sourceFile.close()
    return entries


def writeEntriesTo(entries, target):
    targetFile = open(target, 'w')
    for singleEntry in entries:
        targetFile.write("%s\n" % singleEntry)
    targetFile.close()


def printCopyright():
    print ("Copyright (C) 2022 ErikWe")
    print ("This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions.")
    print ("Please see License: GNU General Public License v3.0")
    print ("===================================================")


if __name__ == "__main__":
    main()
