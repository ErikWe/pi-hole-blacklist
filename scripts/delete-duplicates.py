#Copyright (C) 2022 ErikWe
from collections import OrderedDict

def main():
    printCopyright()
    print ("Remove duplicates from blacklist:")
    print ("Please enter your blacklist: ")
    blacklist = input("source-blacklist: ")
    entries = readEntriesFrom(blacklist)
    duplicateFreeEntries = removeDuplicates(entries)
    writeEntriesTo(duplicateFreeEntries, blacklist)


def removeDuplicates(entries):
    entriesWithDuplicates = len(entries)
    entriesWithoutDuplicates = list(dict.fromkeys(entries))
    removedDuplicates = entriesWithDuplicates - len(entriesWithoutDuplicates)
    print ("Duplicates removed from blacklist:", removedDuplicates)
    return entriesWithoutDuplicates


def readEntriesFrom(source):
    sourceFile = open(source)
    entries = sourceFile.read().splitlines()
    sourceFile.close()
    return entries


def writeEntriesTo(entries, target):
    targetFile = open(target, 'w')
    for singleEntry in entries:
        targetFile.write("%s\n" % singleEntry)
    targetFile.close()


def printCopyright():
    print ("Copyright (C) 2022 ErikWe")
    print ("This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions.")
    print ("Please see License: GNU General Public License v3.0")
    print ("===================================================")


if __name__ == "__main__":
    main()
