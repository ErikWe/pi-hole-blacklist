#Copyright (C) 2022 ErikWe
from collections import OrderedDict

def main():
    printCopyright()
    print ("Sort blacklist:")
    print ("Please enter your blacklist: ")
    blacklist = input("source-blacklist: ")
    entries = readEntriesFrom(blacklist)
    entries.sort()
    writeEntriesTo(entries, blacklist)
    print ("Successfully sorted the blacklist")


def readEntriesFrom(source):
    sourceFile = open(source)
    entries = sourceFile.read().splitlines()
    sourceFile.close()
    return entries


def writeEntriesTo(entries, target):
    targetFile = open(target, 'w')
    for singleEntry in entries:
        targetFile.write("%s\n" % singleEntry)
    targetFile.close()


def printCopyright():
    print ("Copyright (C) 2022 ErikWe")
    print ("This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions.")
    print ("Please see License: GNU General Public License v3.0")
    print ("===================================================")


if __name__ == "__main__":
    main()
