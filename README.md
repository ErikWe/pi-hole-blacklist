# Pi-hole Blacklist

From the official Pi-hole [website](https://docs.pi-hole.net/): 'The Pi-hole® is a DNS sinkhole that protects your devices from unwanted content, without installing any client-side software'. Basically the Pi-hole gets DNS calls from your network, checks if the hostname is on a blacklist/whitelist and depending on your rules, blocks the DNS call or redirects it to a 'real' DNS. This project offers not only blacklists to easily block millions of potentially malicious sites or advertisements but also scripts and tools to create, merge, alter or clean your own blacklists.

## Table of Contents
1. [Getting Started](#Getting-Started)
2. [How to Install and Run the Project](#How-to-Install-and-Run-the-Project)
3. [How to Use the Project](#How-to-Use-the-Project)
4. [Credits](#Credits)
5. [License](#License)

## Getting Started

### Motivation
When setting up and using the Pi-hole for the first time, a set of hostnames to block is required. Searching the internet for existing files will get some blacklists but in the end, all of them need to be added to the Pi-hole manually. In addition, a so-called 'gravity update' will fetch the latest changes from all blacklists linked to the Pi-hole. The benefit of this functionality is to get new security-relevant entries into your Pi-hole but it also opens the door for the blacklist administrators to remove entries from their list. This approach tries to tackle several of these problems. It offers predefined blacklists similar to other sites as well. In addition, this project also supplies the tools and scripts to alter blacklists which sizes are too big to operate on them manually.

So what's the deal with this project?

| What's to Gain From This Project | What are the Project's drawbacks |                          
|---|---|
| One blacklist to manually add to the Pi-hole | No real-time updates regarding security relevant blacklist entries |
| Easy overview of all entries inside the blacklist | Currently no separation of entries into different categories (Advertisement, Security, ...) |
| Full control of the blacklist and its changes | As a result of merging many blacklists, some entries do not make sense |
| Blacklist versioning due to Git |                                                                                      
| Scripts to add/delete/modify blacklists |

In the end it is your decision to use the lists provided for your Pi-hole or for anything else. However, the scripts alone may help you to create your own lists to boost up your Pi-hole and to gain more control over what you are blocking.

### Disclaimer
Before using this project, the lists or the code provided, please ensure that you are aware of what you are doing. I will not take any responsibilities regarding the scripts functionality or the content of the blacklists. Over two million entries from more than thirty lists are too many to observe manually. Please keep in mind that due to the context and nature of this project, some entries on the blacklists provided may contain inappropriate language such as curse-words, nudity or politically incorrect statements. It is important to understand that these entries are used to block traffic to these sites, **not** to promote its content. Before reporting this repository because of certain entries on the blacklist, please contact me and depending on the case, I will remove the specific entry or provide a reason why this entry is required on the list.

### How to Contribute to This Project
If you want to contribute to this projects with your own blacklist or with a set of scripts which might be useful in this regard, feel free to contact me. In addition, if you identify any errors, a quick notice would be nice.

## How to Install and Run the Project
There are two different ways to install and run the project. The first is to download the blacklists and scripts in order to operate on/with them. The other way is to utilize the blacklists by adding them to the Pi-hole. Both ways are described in the following.

### Install the Project and Operate on Blacklists
To install the project, clone the repository into any directory. This process may take a moment due to the size of the repository. After the cloning was successful, the code can be used without any other requirements. The following chapter contains tips on how to use the scripts for custom blacklists and whats required to run them.

```git clone https://gitlab.com/ErikWe/pi-hole-blacklist.git```

### Run the Project in Your Pi-hole
To run the projects blacklists in a Pi-hole, ensure that admin access to the Pi-hole is available. After a successful login into the admin control panel, open the Adlist section by clicking on Group Management and then on Adlist.

<center><img src="./images/AddAdlist1.png" alt="Open Adlist Section" height="400"/></center>

As soon as the section is open, enter the raw GitLab blacklist such as: ```https://gitlab.com/ErikWe/pi-hole-blacklist/-/raw/main/blacklists/external_blacklist.txt```. Optionally, a description for further identification can be added to the entry. To add the blacklist, click on the Add-button.

<center><img src="./images/AddAdlist2.png" alt="Add Blacklist to Adlists"/></center>

After adding the raw blacklist link, the list in the bottom of the section will display the newly added blacklist with a grey question mark.

<center><img src="./images/AddAdlist3.png" alt="Update Gravity to Enable Changes"/></center>

To enable the blacklist and download all the entries to the Pi-hole, perform the gravity update using ```pihole -g```. Keep in mind that this will also update all the other blacklists in this section. After a successful gravity update, the blacklist should be displayed with a green check mark.

## How to Use the Project
This section covers examples for the scripts of this project.

### sort-blacklist.py
The task of this script is to take a blacklist and sort all its entries alphabetically, including comments. This is done to give a blacklist some structure in order to ease the way when looking for a specific entry. Keep in mind that entries which share the same context (same root-domain) such as ```vid.xyz.com``` and ```img.xyz.com``` will not be placed next to each other and thus, clustering the entries based on their root-domain or top-level-domain is a feature which still needs to be developed.

#### Example input:
```
0.0.0.0 banana.com
0.0.0.0 apple.eu
0.0.0.0 cinnamon.de
# the next top-level-domain is chinese
0.0.0.0 dragonfruit.cn
```

#### Example output:
```
# the next top-level-domain is chinese
0.0.0.0 apple.eu
0.0.0.0 banana.com
0.0.0.0 cinnamon.de
0.0.0.0 dragonfruit.cn
```

### delete-duplicates.py
This script deletes duplicates as the following example shows:

#### Example input:
```
0.0.0.0 1.xyz.de
0.0.0.0 11.xyz.de
0.0.0.0 2.xyz.de
0.0.0.0 3.xyz.de
0.0.0.0 3.xyz.de
0.0.0.0 4.xyz.de
0.0.0.0 1.xyz.de
```

#### Example output:
```
0.0.0.0 1.xyz.de
0.0.0.0 11.xyz.de
0.0.0.0 2.xyz.de
0.0.0.0 3.xyz.de
0.0.0.0 4.xyz.de
```

### consume-external-blacklist.py
Existing blacklists come in a variety if formats. They can have leading IP addresses such as ```0.0.0.0 xyz.de``` or ```127.0.0.1 234.yza```. They can have comments behind their entries such as ```www.xyz.de#thisIsAGermanDomain``` or comments which span one or multiple lines. They might be sorted in a weird way or just very good organized. Usually already existing blacklistst has a unique format for themselves but it rarely happens that they contain totally mixed formats.

To restore order, this script takes two files as input, one new blacklist and one blacklist into which it merges the parsed entries from the first one.

The following steps are applied to each entry of the source-file in order to extract a clean entry:

- Remove all empty lines
- Remove all IP addresses (Important as it removes possibly malitious IP addresses)
- Remove all spaces (\s+)
- Remove all comments (#.\*)
- Add custom 0.0.0.0 address in front of each entry

After each entry of the source-file is parsed or removed, the target-file is imported and merged before overwriting all entries in the target-file with the new merge result.
Keep in mind that 

#### Example input1 (source-file):
```
0.0.0.0 1.xyz.de
11.xyz.de#comment
#another comment before empty lines



-ff2.xyz.de
xyz.de
0.0.0.0 3.xyz.de
333.222.111.222 4.xyz.de
0.0.0.0 1.xyz.de
```

#### Example input2 (target-file):
```
0.0.0.0 1.xyz.de
0.0.0.0 2.xyz.de
0.0.0.0 3.xyz.de
```

#### Example output:
```
0.0.0.0 1.xyz.de
0.0.0.0 11.xyz.de
0.0.0.0 -ff2.xyz.de
0.0.0.0 xyz.de
0.0.0.0 3.xyz.de
0.0.0.0 4.xyz.de
0.0.0.0 1.xyz.de
0.0.0.0 1.xyz.de
0.0.0.0 2.xyz.de
0.0.0.0 3.xyz.de
```

## Credits
As the main blacklist is derived from many other lists already existing on the internet, the following table lists the authors and a link to the related list in order to give some credits to the original creators.

| Author | Link to the blacklist |                          
|---|---|
| Abuse.ch | https://urlhaus.abuse.ch/downloads/hostfile/ |
| AdAway | https://adaway.org/hosts.txt |
| Anudeep | https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt |
| Bigdargon | https://raw.githubusercontent.com/bigdargon/hostsVN/master/hosts |
|Chadmayfield | https://github.com/chadmayfield/pihole-blocklists/raw/master/lists/pi_blocklist_porn_all.list |
| DandelionSprout | https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Alternate%20versions%20Anti-Malware%20List/AntiMalwareHosts.txt |
| Digitalside.it | https://osint.digitalside.it/Threat-Intel/lists/latestdomains.txt |
| Disconnect.me | https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt |
| Disconnect.me | https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt |
| Disconnect.me | https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt |
| Ethanr | https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/Mandiant_APT1_Report_Appendix_D.txt |
| FadeMind | https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Spam/hosts |
| FadeMind | https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Risk/hosts |
| FadeMind | https://raw.githubusercontent.com/FadeMind/hosts.extras/master/UncheckyAds/hosts |
| FadeMind | https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.2o7Net/hosts |
| Firebog | https://v.firebog.net/hosts/static/w3kbl.txt |
| Firebog | https://v.firebog.net/hosts/AdguardDNS.txt |
| Firebog | https://v.firebog.net/hosts/Easylist.txt |
| Firebog | https://v.firebog.net/hosts/Easyprivacy.txt |
| Firebog | https://v.firebog.net/hosts/Prigent-Ads.txt |
| Firebog | https://v.firebog.net/hosts/Prigent-Malware.txt |
| Firebog | https://v.firebog.net/hosts/Shalla-mal.txt |
| Firebog | https://v.firebog.net/hosts/Kowabit.txt |
| Frogeye | https://hostfiles.frogeye.fr/firstparty-trackers-hosts.txt |
| GameIndustry.eu | https://github.com/KodoPengin/GameIndustry-hosts-Template/blob/master/Gaming-Full-Template/hosts |
| Jdlingyu | https://raw.githubusercontent.com/jdlingyu/ad-wars/master/hosts |
| Joewein.net | https://www.joewein.net/dl/bl/dom-bl-base.txt |
| Peter Lowe | https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext |
| Phishing.army | https://phishing.army/download/phishing_army_blocklist_extended.txt |
| PolishFiltersTeam | https://raw.githubusercontent.com/PolishFiltersTeam/KADhosts/master/KADhosts_without_controversies.txt |
| Quidsup | https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt |
| Quidsup | https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-malware.txt |
| StevenBlack | https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts |
| Sysctl.org | http://sysctl.org/cameleon/hosts |
| Zerodot1 | https://zerodot1.gitlab.io/CoinBlockerLists/hosts_browser |

## License

pi-hole-blacklist
Copyright (C) 2023  ErikWe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
